# Grizzly Front End Dev Evaluation

This evaluation is meant to explore your techniques and code used in the construction of a very graphically simple website. In effect we are looking for a properly styled and coded responsive realization of the included comp using only the files made available. All design elements are css-generated with the exception of the Instagram feed. Please make note that attention to detail is paramount in this exercise. You are free to use any frameworks you wish, but make sure to keep your code clean. 

The following list of specific techniques requested:

#### Files and languages
- The evaluation should be built using the system set up within this repo, namely SASS (Compass, Bourbon if you like), HTML5 and JS.
- Dealing with breakpoints is up to you, so long as it works and is mobile-first. 
- Lorum Ipsum should be used for text content and placebear.com should be used for background images/slider images. 
- FontAwesome should be used for social icons
- Finally, the Instagram grid should pull a real Instagram feed. If an API key is needed, you may use your own feed, a dummy feed or our test feed (username: **`grizzlytest`** and password: **`GZT3st`**)


#### Image slider
Simple slideshow including 3 slides. We use Owl slider, but feel free to use a slider of your choosing. Heading & text should have subtle animate and/or fade in.

#### Navigation
Nav should stick and shrink in height when it reaches top of the frame.

#### Main Panel w/ Info & graphic
Panel with left text and image on right. Text/image should degrade gracefully. 

#### Centered Panel
Heading, paragraph text & button should center horizontally and vertically.

#### Contact Form
Contact form design should be consistent across all platforms to the extent of cross- browser/device compatibility. 

#### Form Styling
Form should be styled as close as possible to the exact flat style shown here, not using the default browser/device styling.

#### Instagram feed
Dynamically displays the latest six images from an Instagram account.

#### Nav
Nav and rest of page should “cover” feature slider as you scroll. Nav should stick and shrink in height when it reaches top.